function NewWindow(html, Javascript, wid, hei){
    var openPrint = window.open("", "","width="+wid+",height="+hei);
    openPrint.onload = function() {
        var Page = openPrint.document;

        var newDiv = Page.createElement("div");
        var Scripts = Page.createElement("script")
        Scripts.innerHTML = Javascript;
        newDiv.id = 'PLAYER';
        newDiv.innerHTML = html;
        Page.body.appendChild(newDiv);
        Page.body.appendChild(Scripts);
    };
}
